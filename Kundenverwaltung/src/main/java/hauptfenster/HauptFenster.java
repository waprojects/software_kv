package hauptfenster;

import java.awt.EventQueue;

import javax.swing.JFrame;

import einstellungen.EinstellungenFenster;
import kundendatenFenster.kundeAnlegenFenster;
import kundendatenFenster.kundeSuchenFenster;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JSeparator;
import javax.swing.JMenuBar;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.border.MatteBorder;

public class HauptFenster {

	private JFrame frmKundenverwaltung;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HauptFenster window = new HauptFenster();
					window.frmKundenverwaltung.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public HauptFenster() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmKundenverwaltung = new JFrame();
		frmKundenverwaltung.setTitle("Kundenverwaltung");
		frmKundenverwaltung.setBounds(100, 100, 660, 429);
		frmKundenverwaltung.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmKundenverwaltung.getContentPane().setLayout(null);
		frmKundenverwaltung.setLocationRelativeTo(null); //Zentriert das Fenster im Bildschirm.
		
		EinstellungenFenster EF = new EinstellungenFenster();
		
		
		JButton buttonKundeAnlegen = new JButton("Kunde anlegen");
		buttonKundeAnlegen.setFont(EF.buttonFont);
		buttonKundeAnlegen.setBackground(EF.buttonColor);
		buttonKundeAnlegen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				/**
				 * Öffnet das KundeAnlegenFenster ohne Argumente				 * 
				 */
				
				kundeAnlegenFenster eingabe = new kundeAnlegenFenster("","");
				eingabe.main("","");
				frmKundenverwaltung.dispose();
			}
		});
		buttonKundeAnlegen.setBounds(63, 208, 227, 65);
		frmKundenverwaltung.getContentPane().add(buttonKundeAnlegen);
		
		JButton buttonKundeSuchen = new JButton("Kunde suchen");
		buttonKundeSuchen.setFont(EF.buttonFont);
		buttonKundeSuchen.setBackground(EF.buttonColor);
		buttonKundeSuchen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				/**
				 * Öffnet das KundeSuchenFenster 
				 */
				
				kundeSuchenFenster suche = new kundeSuchenFenster();
				suche.main(null);
				frmKundenverwaltung.dispose();
			}
		});
		buttonKundeSuchen.setBounds(353, 208, 227, 65);
		frmKundenverwaltung.getContentPane().add(buttonKundeSuchen);
		
		JButton buttonBuchungAnlegen = new JButton("Buchung anlegen");
		buttonBuchungAnlegen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		buttonBuchungAnlegen.setFont(EF.buttonFont);
		buttonBuchungAnlegen.setBackground(EF.buttonColor);
		buttonBuchungAnlegen.setBounds(63, 26, 227, 65);
		frmKundenverwaltung.getContentPane().add(buttonBuchungAnlegen);
		
		JButton buttonCRM = new JButton("Kundenbeziehungsmaßnahmen");
		buttonCRM.setFont(EF.buttonFont);
		buttonCRM.setBackground(EF.buttonColor);
		buttonCRM.setBounds(63, 299, 227, 65);
		frmKundenverwaltung.getContentPane().add(buttonCRM);
		
		JButton buttonRechnungErzeugen = new JButton("Rechnung erzeugen");
		buttonRechnungErzeugen.setFont(EF.buttonFont);
		buttonRechnungErzeugen.setBackground(EF.buttonColor);
		buttonRechnungErzeugen.setBounds(63, 117, 227, 65);
		frmKundenverwaltung.getContentPane().add(buttonRechnungErzeugen);
		
		JButton buttonEinstellungen = new JButton("Einstellungen");
		buttonEinstellungen.setFont(EF.buttonFont);
		buttonEinstellungen.setBackground(EF.buttonColor);
		buttonEinstellungen.setBounds(353, 299, 227, 65);
		frmKundenverwaltung.getContentPane().add(buttonEinstellungen);
		
		JButton buttonBuchungSuchen = new JButton("Buchung suchen");
		buttonBuchungSuchen.setFont(EF.buttonFont);
		buttonBuchungSuchen.setBackground(EF.buttonColor);
		buttonBuchungSuchen.setBounds(353, 26, 227, 65);
		frmKundenverwaltung.getContentPane().add(buttonBuchungSuchen);
		
		JButton buttonRechnungSuchen = new JButton("Rechnung suchen");
		buttonRechnungSuchen.setFont(EF.buttonFont);
		buttonRechnungSuchen.setBackground(EF.buttonColor);
		buttonRechnungSuchen.setBounds(353, 117, 227, 65);
		frmKundenverwaltung.getContentPane().add(buttonRechnungSuchen);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(45, 102, 550, 4);
		frmKundenverwaltung.getContentPane().add(separator);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(45, 193, 550, 4);
		frmKundenverwaltung.getContentPane().add(separator_1);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(45, 284, 550, 4);
		frmKundenverwaltung.getContentPane().add(separator_2);
	}
}
