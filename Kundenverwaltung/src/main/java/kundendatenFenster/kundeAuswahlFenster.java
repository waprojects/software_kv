package kundendatenFenster;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import einstellungen.EinstellungenFenster;
import javax.swing.JButton;
import net.miginfocom.swing.MigLayout;

public class kundeAuswahlFenster {

	private JFrame frmAuswahl;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					kundeAuswahlFenster window = new kundeAuswahlFenster();
					window.frmAuswahl.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public kundeAuswahlFenster() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmAuswahl = new JFrame();
		frmAuswahl.setTitle("Auswahl");
		frmAuswahl.setBounds(100, 100, 630, 138);
		frmAuswahl.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAuswahl.setLocationRelativeTo(null); // Zentriert das Fenster im Bildschirm.
		
		EinstellungenFenster EF = new EinstellungenFenster();
		frmAuswahl.getContentPane().setLayout(new MigLayout("", "[100px][100px][100px][100px][100px][100px]", "[47px][14.00px][]"));

		//ComboBox Auswahl Kunde:
		final JComboBox comboBoxAuswahlKunde = new JComboBox();		
		//Hier ein Array mit möglichen Ergebnissen, die je nach Auswahl der ComboBox einen anderen Kunden zeigen
		comboBoxAuswahlKunde.setModel(new DefaultComboBoxModel(new String[] {"Ergebnisse auswählen:", "Testkunde1", "Testkunde2"}));
		comboBoxAuswahlKunde.setBackground(EF.buttonColor);
		frmAuswahl.getContentPane().add(comboBoxAuswahlKunde, "cell 0 0 6 1,grow");

		//Button Zurück:
		JButton btnZurck = new JButton("Zurück");
		btnZurck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				kundeSuchenFenster kSF = new kundeSuchenFenster();
				kSF.main(null);
				frmAuswahl.dispose();
			}
		});
		btnZurck.setBackground(EF.buttonColor);
		frmAuswahl.getContentPane().add(btnZurck, "cell 0 2,growx,aligny top");
		
		//Button Anzeigen:
		JButton buttonAnzeigen = new JButton("Anzeigen");
		buttonAnzeigen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/**
				 * Die Auswahl der ComboBoxAuswahlKunde öffnet das Anlegen Fenster mit gefüllten Daten.
				 */
				
				kundeAnlegenFenster kAF = new kundeAnlegenFenster("anzeigen", "");
				//kAF.main("anzeigen", comboBoxAuswahlKunde.getSelectedItem().toString()); //der angezeigte wert muss kdnr enthalten, nur diese soll weitergegeben werden.
				kAF.main("anzeigen", ""); //der angezeigte wert muss kdnr enthalten, nur diese soll weitergegeben werden.	
				
			}
		});
		buttonAnzeigen.setBackground(EF.buttonColor);
		frmAuswahl.getContentPane().add(buttonAnzeigen, "cell 5 2,growx,aligny top");
	}

}
