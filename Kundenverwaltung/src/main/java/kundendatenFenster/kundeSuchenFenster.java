package kundendatenFenster;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import einstellungen.EinstellungenFenster;
import hauptfenster.HauptFenster;

import javax.swing.JButton;
import net.miginfocom.swing.MigLayout;

public class kundeSuchenFenster {

	private JFrame frmKundenSuchen;
	private JTextField textFieldNachname;
	private JTextField textFieldVorname;
	private JTextField textFieldKundennummer;
	private JTextField textFieldStrHsNr;
	private JTextField textFieldPLZ;
	private JTextField textFieldOrt;
	private JTextField textFieldEmail;
	private JTextField textFieldGeburtstag;
	private JTextField textFieldAnsprache;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					kundeSuchenFenster window = new kundeSuchenFenster();
					window.frmKundenSuchen.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public kundeSuchenFenster() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmKundenSuchen = new JFrame();
		frmKundenSuchen.getContentPane().setEnabled(false);
		frmKundenSuchen.setTitle("Kunden suchen");
		frmKundenSuchen.setBounds(100, 100, 630, 312);
		frmKundenSuchen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmKundenSuchen.setLocationRelativeTo(null); //Zentriert das Fenster im Bildschirm.
		
		EinstellungenFenster EF = new EinstellungenFenster();
		frmKundenSuchen.getContentPane().setLayout(new MigLayout("", "[100px][100px][100px][100px][100px][100px]", "[20px][20px][20px][20px][20px][20px][20px][20px][20px][23px][]"));

		// Label Ansprache:
		JLabel labelAnsprache = new JLabel("Ansprache:");
		frmKundenSuchen.getContentPane().add(labelAnsprache, "cell 0 0 3 1,growx,aligny top");

		// Textfeld Ansprache:
		textFieldAnsprache = new JTextField();
		textFieldAnsprache.setColumns(10);
		frmKundenSuchen.getContentPane().add(textFieldAnsprache, "cell 3 0 3 1,growx,aligny top");

		// Label Nachname:
		JLabel labelNachname = new JLabel("Nachname:");
		frmKundenSuchen.getContentPane().add(labelNachname, "cell 0 1 3 1,growx,aligny center");

		// Textfeld Nachname:
		textFieldNachname = new JTextField();
		textFieldNachname.setColumns(10);
		frmKundenSuchen.getContentPane().add(textFieldNachname, "cell 3 1 3 1,growx,aligny top");

		// Label Vorname:
		JLabel labelVorname = new JLabel("Vorname:");
		frmKundenSuchen.getContentPane().add(labelVorname, "cell 0 2 3 1,growx,aligny center");

		// Textfeld Vorname:
		textFieldVorname = new JTextField();
		textFieldVorname.setColumns(10);
		frmKundenSuchen.getContentPane().add(textFieldVorname, "cell 3 2 3 1,growx,aligny top");

		// Label Kundennummer:
		JLabel labelKundennummer = new JLabel("Kundennummer:");
		frmKundenSuchen.getContentPane().add(labelKundennummer, "cell 0 3 3 1,growx,aligny center");

		// Textfeld Kundennummer:
		textFieldKundennummer = new JTextField();
		textFieldKundennummer.setColumns(10);
		frmKundenSuchen.getContentPane().add(textFieldKundennummer, "cell 3 3 3 1,growx,aligny top");

		// Label Straße & Hausnummer:
		JLabel labelStrHsNr = new JLabel("Stra\u00DFe & Hausnummer:");
		frmKundenSuchen.getContentPane().add(labelStrHsNr, "cell 0 4 3 1,growx,aligny center");

		// Textfeld Straße & Hausnummer:
		textFieldStrHsNr = new JTextField();
		textFieldStrHsNr.setEnabled(false);
		textFieldStrHsNr.setColumns(10);
		frmKundenSuchen.getContentPane().add(textFieldStrHsNr, "cell 3 4 3 1,growx,aligny top");

		// Label Postleitzahl:
		JLabel labelPLZ = new JLabel("Postleitzahl:");
		frmKundenSuchen.getContentPane().add(labelPLZ, "cell 0 5 3 1,growx,aligny center");

		// Textfeld Postleitzahl:
		textFieldPLZ = new JTextField();
		textFieldPLZ.setEnabled(false);
		textFieldPLZ.setColumns(10);
		frmKundenSuchen.getContentPane().add(textFieldPLZ, "cell 3 5 3 1,growx,aligny top");

		// Label Ort:
		JLabel labelOrt = new JLabel("Ort:");
		frmKundenSuchen.getContentPane().add(labelOrt, "cell 0 6 3 1,growx,aligny center");

		// Textfeld Ort:
		textFieldOrt = new JTextField();
		textFieldOrt.setEnabled(false);
		textFieldOrt.setColumns(10);
		frmKundenSuchen.getContentPane().add(textFieldOrt, "cell 3 6 3 1,growx,aligny top");

		// Label E-Mail:
		JLabel labelEmail = new JLabel("E-Mail:");
		frmKundenSuchen.getContentPane().add(labelEmail, "cell 0 7 3 1,growx,aligny center");

		// Textfeld E-Mail:
		textFieldEmail = new JTextField();
		textFieldEmail.setEnabled(false);
		textFieldEmail.setColumns(10);
		frmKundenSuchen.getContentPane().add(textFieldEmail, "cell 3 7 3 1,growx,aligny top");

		// Label Geburtstag:
		JLabel labelGeburtstag = new JLabel("Geburtstag:");
		frmKundenSuchen.getContentPane().add(labelGeburtstag, "cell 0 8 3 1,growx,aligny center");

		// Textfeld Geburtstag:
		textFieldGeburtstag = new JTextField();
		textFieldGeburtstag.setEnabled(false);
		textFieldGeburtstag.setColumns(10);
		frmKundenSuchen.getContentPane().add(textFieldGeburtstag, "cell 3 8 3 1,growx,aligny top");
		
		//Button Zurück:
		JButton btnZurueck = new JButton("Zur\u00FCck");
		btnZurueck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/**
				 * Bei Zurück wird das Hauptfenster wieder geöffnet und das aktuelle geschlossen.
				 */
				
				HauptFenster mw = new HauptFenster();
				mw.main(null);
				frmKundenSuchen.dispose();
			}
		});
		btnZurueck.setBackground(EF.buttonColor);
		frmKundenSuchen.getContentPane().add(btnZurueck, "cell 0 10,growx,aligny top");
		
		//Button Suchen:
		JButton btnSuchen = new JButton("Suchen");
		btnSuchen.setBackground(EF.buttonColor);
		btnSuchen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				/**
				 * Bei Drücken auf Suchen soll die Kundendatenbank nach den Attributen durchsucht werden
				 * und ein Array mit den Ergebnissen wird zurückgegeben.
				 * Dieses Array wird dem Aufruf des KundenAnzeigenFensters mitgegeben.
				 */
				
				/**Ali:
				hier methode, die nach Kunden sucht,bspw mit 'SELECT * WHERE ansprache = Herr', aber suche nach alle attributen möglich
				Alle ergebnisse sollen in einem zweidimensionalen Array gespeichert werden in der Form [Kd.nr] [Nachname, Vorname]
				Dieser Array wird dem kundeAnzeigenFenster mitgegeben und füllt dort die ComboBoxAuswahlKunde.
				array[0] = 2; --> also Kd.nr
				array[0][0] = Mustermann Max;
				usw.
				
				KundeAnzeigen ka = new KundeAnzeigen();
				String[] ErgebnisseKunden = ka.kundeErgebnisse(anrede);
								
				*/
				
				kundeAuswahlFenster kAF = new kundeAuswahlFenster();
				kAF.main(null); //hier array als arg mitgeben: 
				//kAF.main(array);
				
				
				
				frmKundenSuchen.dispose(); //Fenster schließen

			}
		});
		frmKundenSuchen.getContentPane().add(btnSuchen, "cell 5 10,growx,aligny top");
	}
}
