package kundendatenFenster;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import einstellungen.EinstellungenDatenbank;
import einstellungen.EinstellungenFenster;
import hauptfenster.HauptFenster;
import kunde.KundeAnlegen;
import kunde.KundeBearbeiten;
import kunde.KundeLöschen;

import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import com.toedter.calendar.JDateChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.SwingConstants;
import net.miginfocom.swing.MigLayout;
import javax.swing.JEditorPane;

public class kundeAnlegenFenster {

	private JFrame frmKundeAnlegen;
	private JTextField textFieldNachname;
	private JTextField textFieldVorname;
	private JTextField textFieldKdNr;
	private JTextField textFieldOrt1;
	private JTextField textFieldStrHsNr1;
	private JTextField textFieldPLZ1;
	private JTextField textFieldOrt2;
	private JTextField textFieldPLZ2;
	private JTextField textFieldStrHsNr2;
	private JTextField textFieldTelefon;
	private JTextField textFieldTelefonMobil;
	private JTextField textFieldEmail;
	private JTextField textFieldFirma;
	private JTextField textFieldFax;
	private JTextField textFieldEmpfaenger;
	private JTextField textFieldIBAN;
	private JTextField textFieldBIC;
	private JTextField textFieldBank;

	/**
	 * Launch the application.
	 */
	public static void main(String attribut, String Kundennummer) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					kundeAnlegenFenster window = new kundeAnlegenFenster(attribut, Kundennummer);
					if (attribut == "" || attribut == null) {

						KundeAnlegen ka = new KundeAnlegen();
						String id = ka.legeNeuenKundenAn("", "", "");
						window.textFieldKdNr.setText(id);
					}

					window.frmKundeAnlegen.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public kundeAnlegenFenster(String attribut, String Kundennummer) {
		initialize(attribut, Kundennummer);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(String attribut, String Kundennummer) {
		frmKundeAnlegen = new JFrame();
		frmKundeAnlegen.setTitle("Kunde anlegen");
		frmKundeAnlegen.setBounds(100, 100, 630, 468);
		frmKundeAnlegen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmKundeAnlegen.setLocationRelativeTo(null); // Zentriert das Fenster im
														// Bildschirm.
		frmKundeAnlegen.getContentPane()
				.setLayout(new MigLayout("", "[100px][100px][100px][100px][100px][100px]", "[21px][372px][23px]"));

		EinstellungenFenster EF = new EinstellungenFenster();

		// Menü:
		JMenuBar menuBar = new JMenuBar();
		frmKundeAnlegen.getContentPane().add(menuBar, "cell 0 0,alignx left,growy");

		JMenu mnMenue = new JMenu("Menü");
		mnMenue.setBackground(EF.buttonColor);
		menuBar.add(mnMenue);
		mnMenue.setHorizontalAlignment(SwingConstants.LEFT);
		// mnMenue.setBackground(EF.buttonColor);

		// MenüButton:
		JMenuItem mntmErinnerungHinzufgen = new JMenuItem("Erinnerung hinzufügen");
		mnMenue.add(mntmErinnerungHinzufgen);

		// MenüButton:
		JMenuItem mntmEmailSchreiben = new JMenuItem("E-Mail schreiben");
		mnMenue.add(mntmEmailSchreiben);

		// MenüButton:
		JMenuItem mntmKundeBearbeiten = new JMenuItem("Kunde bearbeiten");
		mntmKundeBearbeiten.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				/**
				 * Bei Drücken von Bearbeiten soll das AnlegenFenster geöffnet
				 * werden. Dabei wird das Attribut 'bearbeiten' und die Kd.nr.
				 * des Kunden mitgegeben.
				 */

				kundeAnlegenFenster kAF = new kundeAnlegenFenster("bearbeiten", textFieldKdNr.getText());
				kAF.main("bearbeiten", textFieldKdNr.getText());
				/**
				 * der angezeigte wert muss die kdnr enthalten, nur diese muss
				 * weitergegeben werden.
				 */
				frmKundeAnlegen.dispose();
			}
		});

		// MenüButton:
		JMenu mnKundeExportieren = new JMenu("Kunde exportieren");
		mnMenue.add(mnKundeExportieren);

		// MenüButton:
		JMenuItem mntmAlsPdfdatei = new JMenuItem("als PDF-Datei");
		mnKundeExportieren.add(mntmAlsPdfdatei);
		mnMenue.add(mntmKundeBearbeiten);

		// MenüButton:
		JMenuItem mntmKundeLschen = new JMenuItem("Kunde löschen");
		mntmKundeLschen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/**
				 * Bei Drücken von Löschen wird gefragt ob der Anwender sicher
				 * ist. Wenn ja, dann wird der Kunde gelöscht.
				 */
				// ausgewählten Kunde löschen:

				/*
				 * int i = JOptionPane.showConfirmDialog(null,
				 * "Sind Sie sicher, dass Sie den Kunden löschen möchten?",
				 * "Löschen?", JOptionPane.OK_CANCEL_OPTION); if (i == 0) {
				 * KundeLöschen kl = new KundeLöschen();
				 * kl.bestehendenKundenLoeschen(textFieldKdNr.getText());
				 * 
				 * HauptFenster mw = new HauptFenster(); mw.main(null);
				 * frmKundeAnzeigen.dispose();
				 * 
				 * }
				 */
				JOptionPane.showMessageDialog(null, "Die Funktion 'Kunde löschen' ist im Moment deaktiviert!",
						"Information", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		mnMenue.add(mntmKundeLschen);

		// Tab Panel:
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		frmKundeAnlegen.getContentPane().add(tabbedPane, "cell 0 1 6 1,grow");

		// Tab Allgemein:
		JPanel panelAllgemein = new JPanel();
		tabbedPane.addTab("Allgemein", null, panelAllgemein, null);
		panelAllgemein.setLayout(new MigLayout("", "[300px][300.00px]", "[30px][30px][30px][30px][30px]"));

		// Label Anrede:
		JLabel labelAnrede = new JLabel("Anrede:");
		panelAllgemein.add(labelAnrede, "cell 0 0,alignx left,aligny center");

		// ComboBox Anrede:
		JComboBox comboBoxAnrede = new JComboBox();
		comboBoxAnrede.setBackground(EF.buttonColor);
		comboBoxAnrede.setModel(new DefaultComboBoxModel(new String[] { "Auswahl", "Frau", "Herr" }));
		panelAllgemein.add(comboBoxAnrede, "cell 1 0,growx,aligny center");

		// Label Nachname:
		JLabel labelNachname = new JLabel("Nachname:");
		panelAllgemein.add(labelNachname, "cell 0 1,growx,aligny center");

		// Textfeld Nachname:
		textFieldNachname = new JTextField();
		textFieldNachname.setColumns(10);
		panelAllgemein.add(textFieldNachname, "cell 1 1,growx,aligny center");

		// Label Vorname:
		JLabel labelVorname = new JLabel("Vorname:");
		panelAllgemein.add(labelVorname, "cell 0 2,growx,aligny center");

		// Textfeld Vorname:
		textFieldVorname = new JTextField();
		textFieldVorname.setColumns(10);
		panelAllgemein.add(textFieldVorname, "cell 1 2,growx,aligny center");

		// Label Firma:
		JLabel labelFirma = new JLabel("Firma:");
		panelAllgemein.add(labelFirma, "cell 0 3,growx,aligny center");

		// Textfeld Firma:
		textFieldFirma = new JTextField();
		textFieldFirma.setColumns(10);
		panelAllgemein.add(textFieldFirma, "cell 1 3,growx,aligny center");

		// Label Kundennummer:
		JLabel labelKdNr = new JLabel("Kundennummer:");
		panelAllgemein.add(labelKdNr, "cell 0 4,growx,aligny center");

		// Textfeld Kundennummer:
		textFieldKdNr = new JTextField();
		textFieldKdNr.setEnabled(false);
		textFieldKdNr.setColumns(10);
		panelAllgemein.add(textFieldKdNr, "cell 1 4,growx,aligny center");

		// Tab Adressen:
		JPanel panelAdressen = new JPanel();
		tabbedPane.addTab("Adressen", null, panelAdressen, null);
		panelAdressen.setLayout(
				new MigLayout("", "[300.00px][300px]", "[30px][30px][30px][30px][30px][30px][30px][30px][30px]"));

		// Label Adresse 1:
		JLabel lblAdresse1 = new JLabel("Adresse 1:");
		panelAdressen.add(lblAdresse1, "cell 0 0,alignx left,aligny center");

		// Label Straße & Hausnummer 1:
		JLabel labelStrHsNr1 = new JLabel("Straße & Hausnummer:");
		panelAdressen.add(labelStrHsNr1, "cell 0 1,growx,aligny center");

		// Testfeld Straße & Hausnummer 1:
		textFieldStrHsNr1 = new JTextField();
		textFieldStrHsNr1.setColumns(10);
		panelAdressen.add(textFieldStrHsNr1, "cell 1 1,growx,aligny center");

		// Label PLZ 1:
		JLabel labelPLZ1 = new JLabel("Postleitzahl:");
		panelAdressen.add(labelPLZ1, "cell 0 2,growx,aligny center");

		// Textfeld PLZ 1:
		textFieldPLZ1 = new JTextField();
		textFieldPLZ1.setColumns(10);
		panelAdressen.add(textFieldPLZ1, "cell 1 2,growx,aligny center");

		// Label Ort 1:
		JLabel labelOrt1 = new JLabel("Ort:");
		panelAdressen.add(labelOrt1, "cell 0 3,growx,aligny center");

		// Textfeld Ort 1:
		textFieldOrt1 = new JTextField();
		textFieldOrt1.setColumns(10);
		panelAdressen.add(textFieldOrt1, "cell 1 3,growx,aligny center");

		// Label Adresse 2:
		JLabel lblAdresse2 = new JLabel("Adresse 2:");
		panelAdressen.add(lblAdresse2, "cell 0 5,growx,aligny center");

		// Label Straße & Hausnummer 2:
		JLabel labelStrHsNr2 = new JLabel("Straße & Hausnummer:");
		panelAdressen.add(labelStrHsNr2, "cell 0 6,growx,aligny center");

		// Textfeld Straße & Hausnummer 2:
		textFieldStrHsNr2 = new JTextField();
		textFieldStrHsNr2.setColumns(10);
		panelAdressen.add(textFieldStrHsNr2, "cell 1 6,growx,aligny center");

		// Label PLZ 2:
		JLabel labelPLZ2 = new JLabel("Postleitzahl:");
		panelAdressen.add(labelPLZ2, "cell 0 7,growx,aligny center");

		// Textfeld PLZ 2:
		textFieldPLZ2 = new JTextField();
		textFieldPLZ2.setColumns(10);
		panelAdressen.add(textFieldPLZ2, "cell 1 7,growx,aligny center");

		// Label Ort 2:
		JLabel labelOrt2 = new JLabel("Ort:");
		panelAdressen.add(labelOrt2, "cell 0 8,growx,aligny center");

		// Textfeld Ort 2:
		textFieldOrt2 = new JTextField();
		textFieldOrt2.setColumns(10);
		panelAdressen.add(textFieldOrt2, "cell 1 8,growx,aligny center");

		// Tab Bankdaten:
		JPanel panelBankdaten = new JPanel();
		tabbedPane.addTab("Bankdaten", null, panelBankdaten, null);
		panelBankdaten.setLayout(new MigLayout("", "[300px][300px]", "[30px][30px][30px][30px]"));

		// Label Empfänger:
		JLabel labelEmpfaenger = new JLabel("Empfänger:");
		panelBankdaten.add(labelEmpfaenger, "cell 0 0,growx,aligny top");

		// Textfeld Empfänger:
		textFieldEmpfaenger = new JTextField();
		textFieldEmpfaenger.setColumns(10);
		panelBankdaten.add(textFieldEmpfaenger, "cell 1 0,growx,aligny center");

		// Label IBAN:
		JLabel labelIBAN = new JLabel("IBAN:");
		panelBankdaten.add(labelIBAN, "cell 0 1,growx,aligny top");

		// Textfeld IBAN:
		textFieldIBAN = new JTextField();
		textFieldIBAN.setColumns(10);
		panelBankdaten.add(textFieldIBAN, "cell 1 1,growx,aligny center");

		// Label BIC::
		JLabel labelBIC = new JLabel("BIC:");
		panelBankdaten.add(labelBIC, "cell 0 2,growx,aligny top");

		// Textfeld BIC::
		textFieldBIC = new JTextField();
		textFieldBIC.setColumns(10);
		panelBankdaten.add(textFieldBIC, "cell 1 2,growx,aligny center");

		// Label Bank:
		JLabel labelBank = new JLabel("Bank:");
		panelBankdaten.add(labelBank, "cell 0 3,growx,aligny top");

		// Textfeld Bank:
		textFieldBank = new JTextField();
		textFieldBank.setColumns(10);
		panelBankdaten.add(textFieldBank, "cell 1 3,growx,aligny center");

		// Tab Details:
		JPanel panelDetails = new JPanel();
		tabbedPane.addTab("Details", null, panelDetails, null);
		panelDetails.setLayout(new MigLayout("", "[300px][300px]", "[30px][30px][30px][30px][30px]"));

		// Label Telefon:
		JLabel labelTelefon = new JLabel("Telefon:");
		panelDetails.add(labelTelefon, "cell 0 0,growx,aligny top");

		// Textfeld Telefon:
		textFieldTelefon = new JTextField();
		textFieldTelefon.setColumns(10);
		panelDetails.add(textFieldTelefon, "cell 1 0,growx,aligny center");

		// Label Telefon mobil:
		JLabel labelTelefonMobil = new JLabel("Telefon mobil:");
		panelDetails.add(labelTelefonMobil, "cell 0 1,growx,aligny top");

		// Textfeld Telefon mobil:
		textFieldTelefonMobil = new JTextField();
		textFieldTelefonMobil.setColumns(10);
		panelDetails.add(textFieldTelefonMobil, "cell 1 1,growx,aligny center");

		// Label E-Mail:
		JLabel labelEmail = new JLabel("E-Mail:");
		panelDetails.add(labelEmail, "cell 0 2,growx,aligny top");

		// Textfeld E-Mail:
		textFieldEmail = new JTextField();
		textFieldEmail.setColumns(10);
		panelDetails.add(textFieldEmail, "cell 1 2,growx,aligny center");

		// Label Geburtstag:
		JLabel labelGeburtstag = new JLabel("Geburtstag:");
		panelDetails.add(labelGeburtstag, "cell 0 4,growx,aligny top");

		// Datum-Auswahl Geburtstag:
		final JDateChooser dateChooser = new JDateChooser();
		dateChooser.getJCalendar().setTodayButtonVisible(true);
		dateChooser.getJCalendar().setNullDateButtonVisible(true);
		panelDetails.add(dateChooser, "cell 1 4,growx,aligny center");

		// Label Fax:
		JLabel labelFax = new JLabel("Fax:");
		panelDetails.add(labelFax, "cell 0 3,growx,aligny top");

		// Textfeld Fax:
		textFieldFax = new JTextField();
		textFieldFax.setColumns(10);
		panelDetails.add(textFieldFax, "cell 1 3,growx,aligny center");

		// Tab Sonstiges:
		JPanel panelSonstiges = new JPanel();
		tabbedPane.addTab("Sonstiges", null, panelSonstiges, null);
		panelSonstiges.setLayout(new MigLayout("", "[300px][300px,grow]", "[182px,grow][65px,grow][23px][23px]"));

		// Label Notizen::
		JLabel labelNotizen = new JLabel("Notizen:");
		panelSonstiges.add(labelNotizen, "cell 0 0,growx,aligny top");

		// Textbereich Notizen:
		JEditorPane editorPaneNotizen = new JEditorPane();
		panelSonstiges.add(editorPaneNotizen, "cell 1 0,grow");

		// Label Dokumente:
		JLabel labelDokumente = new JLabel("Dokumente:");
		panelSonstiges.add(labelDokumente, "cell 0 1,growx,aligny top");

		// Textbereich Dokumente:
		JEditorPane editorPaneDokumente = new JEditorPane();
		editorPaneDokumente.setEnabled(false);
		panelSonstiges.add(editorPaneDokumente, "cell 1 1,grow");

		// Button Dokument hinzufügen:
		JButton btnDokumentHinzufgen = new JButton("Dokument hinzufügen");
		btnDokumentHinzufgen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				final JFileChooser fc = new JFileChooser();
				fc.showOpenDialog(fc);
			}
		});
		btnDokumentHinzufgen.setBackground(EF.buttonColor);
		panelSonstiges.add(btnDokumentHinzufgen, "cell 0 2 2 1,growx,aligny top");

		// Button Dokumente verwalten:
		JButton buttonDokumenteVerwalten = new JButton("Dokumente verwalten");
		buttonDokumenteVerwalten.setBackground(EF.buttonColor);
		panelSonstiges.add(buttonDokumenteVerwalten, "cell 0 3 2 1,growx,aligny top");

		// Button Abbruch:
		JButton btnAbbruch = new JButton("Abbruch");
		btnAbbruch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/**
				 * Bei Abbruch wird der angelegte leere Kunde wieder gelöscht.
				 */

				if (attribut == "anzeigen") {
					frmKundeAnlegen.dispose();
					return;
				} else if (attribut == "bearbeiten") {

					kundeAnlegenFenster kAF = new kundeAnlegenFenster("anzeigen", textFieldKdNr.getText());
					// kAF.main("anzeigen",
					// comboBoxAuswahlKunde.getSelectedItem().toString()); //der
					// angezeigte wert muss kdnr enthalten, nur diese soll
					// weitergegeben werden.
					kAF.main("anzeigen", textFieldKdNr.getText()); //
					/**
					 * der angezeigtewert muss kdnr enthalten, nur diese soll
					 * weitergegeben werden.
					 */

					frmKundeAnlegen.dispose();
					return;
				}

				KundeLöschen kl = new KundeLöschen();
				kl.bestehendenKundenLoeschen(textFieldKdNr.getText());

				HauptFenster mw = new HauptFenster();
				mw.main(null);
				frmKundeAnlegen.dispose();
			}
		});
		btnAbbruch.setBackground(EF.buttonColor);
		frmKundeAnlegen.getContentPane().add(btnAbbruch, "cell 0 2,growx,aligny top");

		// Button Speichern:
		JButton buttonSpeichern = new JButton("Speichern");
		buttonSpeichern.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/**
				 * Bei Speichern wird der angelegte leere Kunde um eingegebene
				 * Werte ergänzt (25.03.16: im Moment nur
				 * vorname,nachname,anrede).
				 */

				// Verbindung zur Datenbank herstellen
				EinstellungenDatenbank db_verbindung = new EinstellungenDatenbank();
				db_verbindung.erstelleDatenbankverbindung();

				// Überprüfe, ob Attribute != leer. Wenn != leer, einfügen in
				// DB.
				if (comboBoxAnrede.getSelectedItem().toString() == "Auswahl" && textFieldVorname.getText() == ""
						&& textFieldNachname.getText() == "") {
					KundeBearbeiten kb = new KundeBearbeiten();
					kb.bestehendenKundenBearbeiten(textFieldKdNr.getText(), comboBoxAnrede.getSelectedItem().toString(),
							textFieldVorname.getText(), textFieldNachname.getText());

					// Fenster wird geschlossen und Hauptmenü geöffnet, wenn
					// alle notwendigen Felder befüllt sind:
					frmKundeAnlegen.dispose(); // Fenster schließen
					HauptFenster mw = new HauptFenster();
					mw.main(null);
				}

				// ### WALDI hier muss in der Informationsmaske stehen, dass die
				// Felder leer sind!
				else {
					JOptionPane.showMessageDialog(null,
							"Die Felder 'Anrede', 'Nachname' und 'Vorname' wurden nicht ausgefüllt!", "Information",
							JOptionPane.INFORMATION_MESSAGE);
				}

			}
		});
		buttonSpeichern.setBackground(EF.buttonColor);
		frmKundeAnlegen.getContentPane().add(buttonSpeichern, "cell 5 2,growx,aligny top");

		switch (attribut) {
		case "anzeigen":

			frmKundeAnlegen.setTitle("Kunde anzeigen");
			textFieldKdNr.setText(Kundennummer);

			// Bearbeitung aller Textfelder deaktivieren
			textFieldVorname.setEnabled(false);
			textFieldNachname.setEnabled(false);
			textFieldKdNr.setEnabled(false);
			comboBoxAnrede.setEnabled(false);
			textFieldFirma.setEnabled(false);
			textFieldStrHsNr1.setEnabled(false);
			textFieldPLZ1.setEnabled(false);
			textFieldOrt1.setEnabled(false);
			textFieldStrHsNr2.setEnabled(false);
			textFieldPLZ2.setEnabled(false);
			textFieldOrt2.setEnabled(false);
			textFieldEmpfaenger.setEnabled(false);
			textFieldIBAN.setEnabled(false);
			textFieldBIC.setEnabled(false);
			textFieldBank.setEnabled(false);
			textFieldTelefon.setEnabled(false);
			textFieldTelefonMobil.setEnabled(false);
			textFieldEmail.setEnabled(false);
			textFieldFax.setEnabled(false);
			dateChooser.setEnabled(false);
			editorPaneNotizen.setEnabled(false);
			btnDokumentHinzufgen.setEnabled(false);

			buttonSpeichern.setEnabled(false);
			btnAbbruch.setText("Zurück");
			/**
			 * Ali: hier soll die erste Dimension des Arrays aus
			 * kundeSuchenFensters (Zeile 171) gesucht werden, also die Kd.nr
			 * und alle attribute des kunden als Array zurückgegeben werden:
			 * Dabei soll der Array aber fest sein, bspw array[0] immer
			 * kundennummer, array[3] immer vorname. So ungefähr soll das dann
			 * aussehen:
			 * 
			 * KundeAnzeigen ka = new KundeAnzeigen(); String[] arrayDerKunde =
			 * ka.kundeAnzeigen(array[i]);
			 * 
			 * comboBoxAnsprache.setSelectedItem() = arrayDerKunde [1];
			 * textFieldVorname.setText() = arrayDerKunde [2];
			 * textFieldNachname.setText() = arrayDerKunde [3];
			 * textFieldKdNr.setText() = arrayDerKunde [0];
			 * 
			 */

			break;

		case "bearbeiten":
			mntmKundeBearbeiten.setEnabled(false);
			mnKundeExportieren.setEnabled(false);
			mntmErinnerungHinzufgen.setEnabled(false);
			mntmEmailSchreiben.setEnabled(false);

			// Bearbeitung aller Felder aktivieren
			textFieldVorname.setEnabled(true);
			textFieldNachname.setEnabled(true);
			comboBoxAnrede.setEnabled(true);
			textFieldFirma.setEnabled(true);
			textFieldStrHsNr1.setEnabled(true);
			textFieldPLZ1.setEnabled(true);
			textFieldOrt1.setEnabled(true);
			textFieldStrHsNr2.setEnabled(true);
			textFieldPLZ2.setEnabled(true);
			textFieldOrt2.setEnabled(true);
			textFieldEmpfaenger.setEnabled(true);
			textFieldIBAN.setEnabled(true);
			textFieldBIC.setEnabled(true);
			textFieldBank.setEnabled(true);
			textFieldTelefon.setEnabled(true);
			textFieldTelefonMobil.setEnabled(true);
			textFieldEmail.setEnabled(true);
			textFieldFax.setEnabled(true);
			dateChooser.setEnabled(true);
			editorPaneNotizen.setEnabled(true);
			frmKundeAnlegen.setTitle("Kunde bearbeiten");
			break;
		default:
			mnMenue.setEnabled(false);

			break;
		}
	}
}
