package buchungsdatenFenster;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import net.miginfocom.swing.MigLayout;
import javax.swing.JTextField;
import com.toedter.calendar.JCalendar;

public class buchungAnlegenFenster {

	private JFrame frmBuchungAnlegen;
	private JTextField textField;
	private JLabel labelStartzeit;
	private JLabel labelEnddatum;
	private JLabel labelEndzeit;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					buchungAnlegenFenster window = new buchungAnlegenFenster();
					window.frmBuchungAnlegen.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public buchungAnlegenFenster() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmBuchungAnlegen = new JFrame();
		frmBuchungAnlegen.setTitle("Buchung anlegen");
		frmBuchungAnlegen.setBounds(100, 100, 630, 300);
		frmBuchungAnlegen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmBuchungAnlegen.getContentPane().setLayout(new MigLayout("", "[300px][300px,grow]", "[30px][30px][30px][30px][grow]"));
		
		JLabel labelStartDatum = new JLabel("Startdatum:");
		frmBuchungAnlegen.getContentPane().add(labelStartDatum, "cell 0 0,alignx left");
		
		textField = new JTextField();
		frmBuchungAnlegen.getContentPane().add(textField, "cell 1 0,growx");
		textField.setColumns(10);
		
		labelStartzeit = new JLabel("Startzeit:");
		frmBuchungAnlegen.getContentPane().add(labelStartzeit, "cell 0 1");
		
		labelEnddatum = new JLabel("Enddatum:");
		frmBuchungAnlegen.getContentPane().add(labelEnddatum, "cell 0 2");
		
		labelEndzeit = new JLabel("Endzeit:");
		frmBuchungAnlegen.getContentPane().add(labelEndzeit, "cell 0 3");
	}

}
