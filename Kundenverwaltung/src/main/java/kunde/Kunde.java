package kunde;

import java.util.Date;

public class Kunde {
	String vorname;
	String nachname;
	String anrede;
	String kundenID;
	String strHsNr1;
	String PLZ1;
	String ort1;
	String strHsNr2;
	String PLZ2;
	String ort2;
	String email;
	Date geburtstag;
	String telefon;
	String telefonMobil;
	
	
	
	@Override
	public String toString() {
		
		/*Ausgabe Datum wenn null:
		if (geburtstag != null) {
			SimpleDateFormat sDF = new SimpleDateFormat("dd.MM.yyyy");
			sDF.format(geburtstag);
		}
		*/
		return "Kunde: \nvorname= " + vorname + "\nnachname= " + nachname + "\nansprache= " + anrede + "\nkundenID= "
				+ kundenID + "\nstrHsNr1= " + strHsNr1 + "\nPLZ1= " + PLZ1 + "\nort1= " + ort1 + "\nstrHsNr2= " + strHsNr2
				+ "\nPLZ2= " + PLZ2 + "\nort2= " + ort2 + "\nemail= " + email + "\ngeburtstag= " + geburtstag
				+ "\ntelefon= " + telefon + "\ntelefonMobil= " + telefonMobil;
	}
	public String getTelefonMobil() {
		return telefonMobil;
	}
	public void setTelefonMobil(String telefonMobil) {
		this.telefonMobil = telefonMobil;
	}
	public String getTelefon() {
		return telefon;
	}
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}
	public Date getGeburtstag() {
		return geburtstag;
	}
	public void setGeburtstag(Date date) {
		this.geburtstag = date;
	}
	public String getKundenID() {
		return kundenID;
	}
	public void setKundenID(String kundenID) {
		this.kundenID = kundenID;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getVorname() {
		return vorname;
	}
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	public String getNachname() {
		return nachname;
	}
	public void setNachname(String nachname) {
		this.nachname = nachname;
	}
	public String getAnrede() {
		return anrede;
	}
	public void setAnrede(String ansprache) {
		this.anrede = ansprache;
	}
	public String getStrHsNr1() {
		return strHsNr1;
	}
	public void setStrHsNr1(String strHsNr1) {
		this.strHsNr1 = strHsNr1;
	}
	public String getPLZ1() {
		return PLZ1;
	}
	public void setPLZ1(String pLZ1) {
		PLZ1 = pLZ1;
	}
	public String getOrt1() {
		return ort1;
	}
	public void setOrt1(String ort1) {
		this.ort1 = ort1;
	}
	public String getStrHsNr2() {
		return strHsNr2;
	}
	public void setStrHsNr2(String strHsNr2) {
		this.strHsNr2 = strHsNr2;
	}
	public String getPLZ2() {
		return PLZ2;
	}
	public void setPLZ2(String pLZ2) {
		PLZ2 = pLZ2;
	}
	public String getOrt2() {
		return ort2;
	}
	public void setOrt2(String ort2) {
		this.ort2 = ort2;
	}
	
	
	
}
