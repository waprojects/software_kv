package kunde;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import einstellungen.EinstellungenDatenbank;

public class KundeBearbeiten {

public String bestehendenKundenBearbeiten(String id, String anrede, String vorname, String nachname) {
	
		/**
		 * Aktualisieren des Kunden mit der Kundennummer 'id' um die Werte 'anrede', 'vorname', 'nachname'
		 */
	
		// Umwandlung der Strings in chars, damit PostgreSQL keine Fehler ausgibt
		char[] anredeNeu = new char[anrede.length()];
		anredeNeu = anrede.toCharArray();
		String a = String.copyValueOf(anredeNeu);
		
		char[] vornameNeu = new char[vorname.length()];
		vornameNeu = vorname.toCharArray();
		String b = String.copyValueOf(vornameNeu);
		
		char[] nachnameNeu = new char[nachname.length()];
		nachnameNeu = nachname.toCharArray();
		String c = String.copyValueOf(nachnameNeu);		
			
		
		// Initialisierung der Anmeldeinformationen zur Datenbank
						
				EinstellungenDatenbank einstellungenDB = new EinstellungenDatenbank();
				
				String db_hostname 		= einstellungenDB.hostname;
				String db_portnummer 	= einstellungenDB.portnummer;
				String db_name 			= einstellungenDB.db_name;
				String db_user 			= einstellungenDB.benutzername;
				String db_passwort 		= einstellungenDB.passwort;
				
				
				String db_url = "jdbc:postgresql://"+db_hostname+":"+db_portnummer+"/"+db_name;
				
				
		// ##### Schritt 1: Registrierung des PostgreSQL-Treibers #####
				
				try {
					Class.forName("org.postgresql.Driver");
				} 
				
				catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
				
				
		// ##### Schritt 2: Aufbau der Verbindung zur Datenbank #####
				
				Connection db_connection = null;
				
				try {				
					db_connection = DriverManager.getConnection(db_url,db_user,db_passwort);
				} 
				
				catch (SQLException e) {
					e.printStackTrace();
				}
				
				
		// ##### Schritt 3: Ausführung des SQL-Statents und Erfassen der notwendigen Daten #####
				Statement statement = null;
				
				try {
						
						statement = db_connection.createStatement();
						statement.executeUpdate("UPDATE KUNDEN_1 SET ansprache = '"+a+"', vorname = '"+b+"', nachname = '"+c+"' WHERE kundennummer = '"+id+"';");
					}
					
				catch (SQLException e) {
					
					e.printStackTrace();
				}
				
				
									
		        
				return id;
				
		
	}
}
