package einstellungen;

import java.awt.Color;
import java.awt.Font;

/**
 * Hier sind Variablen für Fenstereinstellungen definiert,
 * sodass Button etc. in jedem Fenster gleich aussehen.
 * @author Waldemar Börkircher
 */
public class EinstellungenFenster {
	
	public Font buttonFont = new Font("Tahoma", Font.BOLD, 12) ;
	public Font MainWindowLabelFont = new Font("Tahoma", Font.PLAIN, 14) ;
	public Color buttonColor = Color.LIGHT_GRAY;
	public String htmlEinstellungLabel = "<html><body style='width: 100%'>";
	
	//Maße - TODO für Button, Label, JFrame
	public String xWert = "";
	public String yWert = "";
	public String weiteWert = "";
	public String hoeheWert = "";
	
}
