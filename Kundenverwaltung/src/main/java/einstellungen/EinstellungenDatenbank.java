package einstellungen;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class EinstellungenDatenbank {

	//Festlegen der Einstellungen für die Datenbank
	public String benutzername = "postgres";
	public String passwort = "abc123";
	public String hostname = "localhost";
	public String portnummer = "5432";
	public String db_name = "postgres";
	
	
	public void erstelleDatenbankverbindung(){
		
		//Initialisierung des JDBC-Treibers (hier von PostgreSQL) und der URL zur Datenbank 		
		String jdbc_driver = "org.postgresql.Driver";
		String db_url = "jdbc:postgresql://"+this.hostname+":"+this.portnummer+"/"+this.db_name; 
		
		//Initialisierung einer Verbindung und eines Befehls mit Standardwerten
		Connection connection = null;
		Statement statement = null;
				
		//Registrierung des JDBC-Treibers	
		try{
			Class.forName(jdbc_driver);
		}
		catch(ClassNotFoundException cnfe){
			cnfe.printStackTrace();
		}

	    //Öffnen der Verbindung	
		try{
			connection = DriverManager.getConnection(db_url,this.benutzername,this.passwort);
			
			if (connection != null) {
				
				//Befehl erstellen
				statement = connection.createStatement();	      
			      
				//Befehl ausführen
				//#Anmerkung: Hier noch die richtigen Datentypen, Bezeichnungen und Verweise auf andere Relationen ändern
				statement.executeUpdate("CREATE TABLE IF NOT EXISTS KUNDEN_1(KUNDENNUMMER serial NOT NULL, ANSPRACHE char(4), VORNAME char(30), NACHNAME char(30), primary key (KUNDENNUMMER));");
			    //statement.executeUpdate("CREATE TABLE IF NOT EXISTS AUFTRAEGE(TICKET_ID char(50) not null, TICKETS_NAME char(50), PROJEKT_ID char(50), ERSTELLER char(50), STATUS char(50), ZEITSTEMPEL char(50), primary key (TICKET_ID), foreign key (PROJEKT_ID) references PROJEKTE (PROJEKT_ID));");
			    //statement.executeUpdate("CREATE TABLE IF NOT EXISTS ADRESSE(AENDERUNGS_ID char(50) not null, TICKET_ID char(50), AENDERER char(50), ZEITSTEMPEL char(50), primary key (AENDERUNGS_ID), foreign key (TICKET_ID) references TICKETS (TICKET_ID));");
			}
		      
		}
		catch(SQLException se){
			se.printStackTrace();
		}
		
		
	     
	      
	}
	

	
}
